# Deep Reinforcement Learning Assignments Repository #

This repositories contains the code for assignment 1 and 2 of the DRL course at CMU. Assignment 1 solves both the stochastic and deterministic FrozenLake environment using policy and value iteration techniques. Assignment 2 solves MountainCar, CartPole and the ATARI Space Invaders game with DQN networks.