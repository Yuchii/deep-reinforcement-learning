#!/usr/bin/env python
import tensorflow as tf, numpy as np, gym, sys, copy, argparse
from scipy.stats import norm
from enum import Enum
from gym import wrappers

verbose = False


def printVerbose(text):
    if verbose:
        print(text)


class network(Enum):
    Linear = 1  # Linear Network
    Deep = 2  # Deep Q Network
    Dueling = 3  # Dueling Network
    DDQN = 4  # Double Dueling Q Network
    Double = 5  # Double Q Network
    Space = 6  # Variation of DDQN for SpaceInvaders


class QNetwork():
    """
    Class to represent the deep Q Network.
    """

    def __init__(self, environment, network_type, learning_rate, layers, advantage_layers, value_layers, update_freq):
        """

        :param environment:         OpenAI Gym Environment
        :param network_type:        Type of network
        :param learning_rate:       Learning rate for training
        :param layers:              Number of units per layer in DQN and in main stream of Dueling DQN.
        :param advantage_layers:    Number of units per layer in advantage stream of Dueling DQN
        :param value_layers:        Number of units per layer in value stream of Dueling DQN
        :param update_freq:         Frequency ot update target network
        """
        self.env = environment
        self.type = network_type
        self.learning_rate = learning_rate
        self.layers = layers
        self.advantage_layers = advantage_layers
        self.value_layers = value_layers
        self.update_freq = update_freq

        self.counter = 0  # Counter member of updating target network
        self.is_double = False  # Bool for networks with online and target

        if self.type == network.Linear:
            self.buildLinearQNetwork()
        elif self.type == network.Deep:
            self.buildDeepQNetwork()
        elif self.type == network.Double:
            self.buildDoubleQNetwork()
            self.is_double = True
        elif self.type == network.Dueling:
            self.buildDuelingNetwork()
        elif self.type == network.DDQN:
            self.buildDDQN()
            self.is_double = True
        elif self.type == network.Space:
            self.buildSpace()
            self.is_double = True
        self.merged = tf.summary.merge_all()

    def save_model_weights(self, save_model):
        """
        Saves the model weights
        :param save_model:  location to save weights
        :return: None
        """
        self.saver.save(self.sess, save_model)

    def init_model(self, model_file):
        """
        Initializes the model
        :param model_file:  Location to save file. Use None for random initialization
        :return:
        """
        self.writer = tf.summary.FileWriter("logs", graph=tf.get_default_graph())

        init = tf.initialize_all_variables()
        self.sess = tf.InteractiveSession()
        self.saver = tf.train.Saver()
        if model_file:
            self.saver.restore(self.sess, model_file)
        else:
            self.sess.run(init)
            self.updateTargetNetwork()

    def getMaxAction(self, state):
        """
        Returns the values for all actions as well as the max action along axis = 1 for state
        :param state:       input state
        :return:            action values, action with max value
        """
        values = self.getValue(state, 0)
        return values, np.expand_dims(np.argmax(values, axis=1), 1)

    def getMaxValue(self, state):
        """
        Returns the values for all actions as well as the max value along axis = 1 for state
        If the network type uses an online and target network, it selects the action with
        the online network and evaluates with the target network
        :param state:       input state
        :return:            action values, max values
        """
        if self.is_double:
            values = self.getValue(state, 0)
            t_vals = np.zeros(values.shape)
            t_vals[np.arange(len(values)), values.argmax(1)] = 1
            e_vals = self.getValue(state, 1)

            return values, np.sum(t_vals * e_vals, axis=1, keepdims=True)
        else:
            values = self.getValue(state)

            return values, np.expand_dims(np.max(values, axis=1), 1)

    def getValue(self, state, i=None):
        """
        Returns the action values for a given state. Double network types must specify the network
        :param state:       input state
        :param i:           network index
        :return:            values
        """
        if self.is_double:
            val = self.sess.run(self.output[i], feed_dict={self.input_state[i]: state,
                                                           self.target: np.zeros(
                                                               (state.shape[i], self.target.get_shape().as_list()[1]))})
        else:
            val = self.sess.run(self.output, feed_dict={self.input_state: state,
                                                        self.target: np.zeros(
                                                            (state.shape[0], self.target.get_shape().as_list()[1]))})
        return val

    def trainDQN(self, state, target):
        """
        Trains the DQN for one iteration. If network is a double architecture, it increments counter and updates target
        network if necessary.
        :param state:       input state
        :param target:      target value
        :return:            None
        """
        if self.is_double:
            merged, _, loss = self.sess.run([self.merged, self.train, self.loss],
                                    feed_dict={self.input_state[0]: state, self.target: target})

            self.counter = (self.counter + 1) % self.update_freq

            if self.counter == 0:
                self.updateTargetNetwork()

        else:
            merged, _, loss = self.sess.run([self.merged, self.train, self.loss], feed_dict={self.input_state: state, self.target: target})

        return merged

    def buildLinearQNetwork(self):
        """
        Builds a MLP with linear activation
        :return:            None
        """
        self.input_state = tf.placeholder(tf.float32, [None] + list(self.env.observation_space.shape),
                                          name='input_state')
        regularizer = tf.contrib.layers.l2_regularizer(scale=0.0001)

        layer = self.input_state
        for i in range(len(self.layers)):
            layer = tf.layers.dense(layer, self.layers[i], kernel_regularizer=regularizer, name = 'FC_Layer_' + str(i))

        self.output = tf.layers.dense(layer, self.env.action_space.n)
        self.target = tf.placeholder(tf.float32, [None, self.env.action_space.n], name='value_target')
        self.loss = tf.losses.mean_squared_error(self.target, self.output)
        self.train = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss)

    def buildDeepQNetwork(self):
        """
        Builds a MLP with ReLU activation
        :return:            None
        """
        self.input_state = tf.placeholder(tf.float32, [None] + list(self.env.observation_space.shape),
                                          name='input_state')
        regularizer = tf.contrib.layers.l2_regularizer(scale=0.0001)

        layer = self.input_state
        for i in range(len(self.layers)):
            layer = tf.layers.dense(layer, self.layers[i], kernel_regularizer=regularizer, name = 'FC_Layer_' + str(i))

        self.output = tf.layers.dense(layer, self.env.action_space.n, name = 'output_layer')
        self.target = tf.placeholder(tf.float32, [None, self.env.action_space.n], name='value_target')
        self.loss = tf.losses.mean_squared_error(self.target, self.output)
        self.train = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss)

    def buildDoubleQNetwork(self):
        """
        Builds a DQN with a target and online network
        :return:            None
        """
        self.input_state = []
        self.output = []

        with tf.variable_scope("online"):
            self.input_state.append(tf.placeholder(tf.float32, [None] + list(self.env.observation_space.shape)))
            regularizer = tf.contrib.layers.l2_regularizer(scale=0.0001)

            layer = self.input_state[0]
            for i in range(len(self.layers)):
                layer = tf.layers.dense(layer, self.layers[i], kernel_regularizer=regularizer,
                                        name='FC_Layer_' + str(i))

            self.output.append(tf.layers.dense(layer, self.env.action_space.n))
            self.target = tf.placeholder(tf.float32, [None, self.env.action_space.n], name='value_target')
            self.loss = tf.losses.mean_squared_error(self.target, self.output[0])
            self.train = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss)

        with tf.variable_scope("target"):
            self.input_state.append(tf.placeholder(tf.float32, [None] + list(self.env.observation_space.shape)))
            regularizer = tf.contrib.layers.l2_regularizer(scale=0.0001)

            layer = self.input_state[1]
            for i in range(len(self.layers)):
                layer = tf.layers.dense(layer, self.layers[i], kernel_regularizer=regularizer,
                                        name='FC_Layer_' + str(i))

            self.output.append(tf.layers.dense(layer, self.env.action_space.n))

        online = tf.trainable_variables('online')
        target = tf.trainable_variables('target')

        self.updateOp = [target[i].assign(online[i]) for i in range(len(target))]

    def buildDuelingNetwork(self):
        """
        Builds a Dueling DQN with value and advantage streams
        :return:            None
        """
        self.input_state = tf.placeholder(tf.float32, [None] + list(self.env.observation_space.shape),
                                          name='input_state')
        regularizer = tf.contrib.layers.l2_regularizer(scale=0.0001)

        layer = self.input_state
        for i in range(len(self.layers)):
            layer = tf.layers.dense(layer, self.layers[i], kernel_regularizer=regularizer, name = 'main_Layer_' + str(i))

        a_layer = layer
        v_layer = layer

        for i in range(len(self.advantage_layers)):
            a_layer = tf.layers.dense(a_layer, self.advantage_layers[i],
                                      tf.nn.relu, kernel_regularizer=regularizer, name = 'adv_Layer_' + str(i))
        a_layer = tf.layers.dense(a_layer, self.env.action_space.n, kernel_regularizer=regularizer, name = 'adv_output')
        tf.summary.histogram('adv_stream', a_layer)

        for i in range(len(self.value_layers)):
            v_layer = tf.layers.dense(v_layer, self.value_layers[i],
                                      tf.nn.relu, kernel_regularizer=regularizer, name = 'val_Layer_' + str(i))
        v_layer = tf.layers.dense(v_layer, 1, kernel_regularizer=regularizer, name = 'val_output')
        tf.summary.histogram('val_stream', v_layer)

        self.output = v_layer + (a_layer - tf.reduce_mean(a_layer, axis=1, keep_dims=True))
        self.target = tf.placeholder(tf.float32, [None, self.env.action_space.n], name='value_target')
        self.loss = tf.losses.mean_squared_error(self.target, self.output)
        self.train = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss)

    def buildDDQN(self):
        """
        Builds a target and online Dueling DQN
        :return:            None
        """
        self.input_state = []
        self.output = []

        with tf.variable_scope("online"):
            self.input_state.append(tf.placeholder(tf.float32, [None] + list(self.env.observation_space.shape), name='online_input'))
            regularizer = tf.contrib.layers.l2_regularizer(scale=0.0001)

            layer = self.input_state[0]
            for i in range(len(self.layers)):
                layer = tf.layers.dense(layer, self.layers[i], kernel_regularizer=regularizer,
                                        name='main_Layer_' + str(i))

            a_layer = layer
            v_layer = layer

            for i in range(len(self.advantage_layers)):
                a_layer = tf.layers.dense(a_layer, self.advantage_layers[i],
                                          tf.nn.relu, kernel_regularizer=regularizer, name='adv_Layer_' + str(i))
            a_layer = tf.layers.dense(a_layer, self.env.action_space.n, kernel_regularizer=regularizer,
                                      name='adv_output')
            tf.summary.histogram('adv_stream', a_layer)

            for i in range(len(self.value_layers)):
                v_layer = tf.layers.dense(v_layer, self.value_layers[i],
                                          tf.nn.relu, kernel_regularizer=regularizer, name='val_Layer_' + str(i))
            v_layer = tf.layers.dense(v_layer, 1, kernel_regularizer=regularizer, name='val_output')
            tf.summary.histogram('val_stream', v_layer)

            self.output.append(v_layer + (a_layer - tf.reduce_mean(a_layer, axis=1, keep_dims=True)))
            self.target = tf.placeholder(tf.float32, [None, self.env.action_space.n])
            self.loss = tf.losses.mean_squared_error(self.target, self.output[0])
            self.train = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss)

        with tf.variable_scope("target"):
            self.input_state.append(tf.placeholder(tf.float32, [None] + list(self.env.observation_space.shape), name='target_input'))
            regularizer = tf.contrib.layers.l2_regularizer(scale=0.0001)

            layer = self.input_state[1]
            for i in range(len(self.layers)):
                layer = tf.layers.dense(layer, self.layers[i], kernel_regularizer=regularizer,
                                        name='main_Layer_' + str(i))

            a_layer = layer
            v_layer = layer

            for i in range(len(self.advantage_layers)):
                a_layer = tf.layers.dense(a_layer, self.advantage_layers[i],
                                          tf.nn.relu, kernel_regularizer=regularizer, name='adv_Layer_' + str(i))
            a_layer = tf.layers.dense(a_layer, self.env.action_space.n, kernel_regularizer=regularizer,
                                      name='adv_output')

            for i in range(len(self.value_layers)):
                v_layer = tf.layers.dense(v_layer, self.value_layers[i],
                                          tf.nn.relu, kernel_regularizer=regularizer, name='val_Layer_' + str(i))
            v_layer = tf.layers.dense(v_layer, 1, kernel_regularizer=regularizer, name='val_output')

            self.output.append(v_layer + (a_layer - tf.reduce_mean(a_layer, axis=1, keep_dims=True)))

        online = tf.trainable_variables('online')
        target = tf.trainable_variables('target')

        self.updateOp = [target[i].assign(online[i]) for i in range(len(target))]

    def buildSpace(self):
        """
        Builds a special version of DDQN for SpaceInvaders
        :return:            None
        """
        self.input_state = []
        self.output = []

        with tf.variable_scope("online"):
            self.input_state.append(tf.placeholder(tf.float32, [None, 105, 80, 4]))
            tf.summary.image('input_image', tf.transpose(self.input_state[0], [3, 1, 2, 0])[:,:,:,0, None], 4)

            conv1 = tf.layers.conv2d(self.input_state[0], 16, 8, [4, 4], activation=tf.nn.relu, name = "conv_layer_1")
            conv2 = tf.layers.conv2d(conv1, 32, 4, [2, 2], activation=tf.nn.relu, name = "conv_layer_2")
            conv_flat = tf.layers.flatten(conv2, name = "layer_flatten")

            regularizer = tf.contrib.layers.l2_regularizer(scale=0.0001)

            layer = conv_flat
            for i in range(len(self.layers)):
                layer = tf.layers.dense(layer, self.layers[i], kernel_regularizer=regularizer,
                                        name='main_Layer_' + str(i))

            a_layer = layer
            v_layer = layer

            for i in range(len(self.advantage_layers)):
                a_layer = tf.layers.dense(a_layer, self.advantage_layers[i],
                                          tf.nn.relu, kernel_regularizer=regularizer, name='adv_Layer_' + str(i))
            a_layer = tf.layers.dense(a_layer, self.env.action_space.n, kernel_regularizer=regularizer,
                                      name='adv_output')
            tf.summary.histogram('adv_stream', a_layer)

            for i in range(len(self.value_layers)):
                v_layer = tf.layers.dense(v_layer, self.value_layers[i],
                                          tf.nn.relu, kernel_regularizer=regularizer, name='val_Layer_' + str(i))
            v_layer = tf.layers.dense(v_layer, 1, kernel_regularizer=regularizer, name='val_output')
            tf.summary.histogram('val_stream', v_layer)

            self.output.append(v_layer + (a_layer - tf.reduce_mean(a_layer, axis=1, keep_dims=True)))
            self.target = tf.placeholder(tf.float32, [None, self.env.action_space.n])
            self.loss = tf.losses.mean_squared_error(self.target, self.output[0])
            self.train = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss)

        with tf.variable_scope("target"):
            self.input_state.append(tf.placeholder(tf.float32, [None, 105, 80, 4]))

            conv1 = tf.layers.conv2d(self.input_state[1], 16, 8, [4, 4], activation=tf.nn.relu)
            conv2 = tf.layers.conv2d(conv1, 32, 4, [2, 2], activation=tf.nn.relu)
            conv_flat = tf.layers.flatten(conv2)

            regularizer = tf.contrib.layers.l2_regularizer(scale=0.0001)

            layer = conv_flat
            for i in range(len(self.layers)):
                layer = tf.layers.dense(layer, self.layers[i], kernel_regularizer=regularizer,
                                        name='main_Layer_' + str(i))

            a_layer = layer
            v_layer = layer

            for i in range(len(self.advantage_layers)):
                a_layer = tf.layers.dense(a_layer, self.advantage_layers[i],
                                          tf.nn.relu, kernel_regularizer=regularizer, name='adv_Layer_' + str(i))
            a_layer = tf.layers.dense(a_layer, self.env.action_space.n, kernel_regularizer=regularizer,
                                      name='adv_output')

            for i in range(len(self.value_layers)):
                v_layer = tf.layers.dense(v_layer, self.value_layers[i],
                                          tf.nn.relu, kernel_regularizer=regularizer, name='val_Layer_' + str(i))
            v_layer = tf.layers.dense(v_layer, 1, kernel_regularizer=regularizer, name='val_output')

            self.output.append(v_layer + (a_layer - tf.reduce_mean(a_layer, axis=1, keep_dims=True)))

        online = tf.trainable_variables('online')
        target = tf.trainable_variables('target')

        self.updateOp = [target[i].assign(online[i]) for i in range(len(target))]


    def updateTargetNetwork(self):
        if self.is_double:
            self.sess.run(self.updateOp)


class Replay_Memory():
    """
    Memory class to hold past experience for experience replay
    """

    def __init__(self, state_size, memory_size, data_type=np.float_):
        """
        :param state_size: Size of the current and input state
        :param memory_size:     Buffer size for experience. Default 500
        :param data_type:       Data type for state. Downgrade to save memory. Default float
        """
        self.memory_size = memory_size
        self.states = np.zeros(([memory_size] + state_size), dtype=data_type)
        self.actions = np.zeros((memory_size, 1), dtype=int)
        self.next_states = np.zeros(([memory_size] + state_size), dtype=data_type)
        self.rewards = np.zeros((memory_size, 1))
        self.terminals = np.full((memory_size, 1), True, dtype=bool)
        self.n_elements = 0
        self.index = 0

    def sample_batch(self, batch_size=32):
        """
        :param batch_size:      batch size
        :return:                states, actions, next_states, rewards,  terminal status
        """
        rand_ind = [np.random.randint(0, min(self.n_elements, self.memory_size)) for i in range(batch_size)]
        return self.states[rand_ind, :], self.actions[rand_ind, :], self.next_states[rand_ind, :], self.rewards[
                                                                                                   rand_ind,
                                                                                                   :], self.terminals[
                                                                                                       rand_ind, :]

    def append(self, state, action, next_state, reward, terminal):
        """
        Adds to experience replay
        :param state:           current state
        :param action:          current action
        :param next_state:      next state
        :param reward:          reward for next state
        :param terminal:        Boolean not of whether next state is terminal
        :return:                None
        """
        self.states[self.index, :] = state
        self.actions[self.index, :] = action
        self.rewards[self.index, :] = reward
        self.next_states[self.index, :] = next_state
        self.terminals[self.index, :] = terminal
        self.index = (self.index + 1) % self.memory_size
        self.n_elements = min(self.n_elements + 1, self.memory_size)


class Replay_Memory_Space():
    """
    Memory class to hold past experience for experience replay
    """

    def __init__(self, state_size, memory_size, data_type=np.float_):
        """
        :param state_size: Size of the current and input state
        :param memory_size:     Buffer size for experience. Default 500
        :param data_type:       Data type for state. Downgrade to save memory. Default float
        """
        self.memory_size = memory_size
        self.states = np.zeros(([memory_size] + state_size), dtype=data_type)
        self.actions = np.zeros((memory_size, 1), dtype=int)
        self.rewards = np.zeros((memory_size, 1))
        self.terminals = np.full((memory_size, 1), True, dtype=bool)
        self.n_elements = 0
        self.index = 0

    def sample_batch(self, batch_size=32):
        """
        :param batch_size:      batch size
        :return:                states, actions, next_states, rewards,  terminal status
        """
        rand_ind = [np.random.randint(0, min(self.n_elements, self.memory_size)) for i in range(batch_size)]
        return self.states[rand_ind, :], self.actions[rand_ind, :], self.rewards[rand_ind, :], self.terminals[rand_ind, :]

    def append(self, state, action, reward, terminal):
        """
        Adds to experience replay
        :param state:           current state
        :param action:          current action
        :param next_state:      next state
        :param reward:          reward for next state
        :param terminal:        Boolean not of whether next state is terminal
        :return:                None
        """
        self.states[self.index, :] = state
        self.actions[self.index, :] = action
        self.rewards[self.index, :] = reward
        self.terminals[self.index, :] = terminal
        self.index = (self.index + 1) % self.memory_size
        self.n_elements = min(self.n_elements + 1, self.memory_size)


class DQN_Agent():
    """
    Agent to step through environment and train DQN
    """

    def __init__(self, environment_name, network_type, learning_rate, layers, update_freq, advantage_layers=None,
                 value_layers=None, discount=1, performance_counter=1000, monitor_dir=None, data_type=None,
                 memory_size=50000):
        """
        :param environment_name:    Name of environment
        :param network_type:        Type of network. Select from network enum
        :param learning_rate:       Learning rate
        :param layers:              Number of layers for network. Use tuple or list
        :param advantage_layers:    Number of layers for advantage stream. Use tuple or list. Default None
        :param value_layers:        Number of layers for value stream. Use tuple or list. Default None
        :param discount:            Discount factor. Default 1
        :param performance_counter: Rate at which to run a performance update. Default 1000
        :param monitor_dir:         Directory to save monitor files. Default None
        """
        self.env = gym.make(environment_name)
        self.test_env = gym.make(environment_name)
        self.network_type = network_type
        self.env.reset()
        self.DQN = QNetwork(self.env, network_type, learning_rate, layers, advantage_layers, value_layers, update_freq)
        self.discount = discount
        self.performance_counter = performance_counter

        if monitor_dir:
            self.test_env = wrappers.Monitor(self.test_env, monitor_dir, video_callable=lambda episode_id: True)

        if network_type == network.Space:
            self.memory = Replay_Memory_Space([105, 80, 5], memory_size, data_type=data_type)
        else:
            self.memory = Replay_Memory(list(self.env.observation_space.shape), memory_size, data_type=data_type)

    def initDQN(self, model_file=None):
        """
        Initializes network. Must be called before training.
        :param model_file:      Location of save file to load. Default None for random initialization.
        :return:                None
        """
        self.DQN.init_model(model_file)

    def epsilon_greedy_policy(self, state, epsilon):
        """
        Chooses an action from policy with probability 1 - epsilon. Samples one from action space with probability
        epsilon
        :param state:           input state
        :param epsilon:         probability epsilon
        :return: values for all actions, action with max value
        """
        action_value, action = self.DQN.getMaxAction(state)
        if np.random.uniform() > epsilon:
            return action_value, action
        else:
            return action_value, np.array(self.env.action_space.sample(), ndmin=2)

    def greedy_policy(self, state):
        """
        Chooses the action from policy with max value
        :param state:           input state
        :return:                action
        """
        return self.DQN.getMaxAction(state)

    def train(self, episodes, max_epsilon, min_epsilon, experience, burn_in, render=False, save_model=None):
        """
        Trains the DQN
        :param episodes:        Number of episodes to train for
        :param max_epsilon:     Max epsilon
        :param min_epsilon:     Min epsilon
        :param experience:      Use experience replay or not
        :param burn_in:         Number of iterations to burn in
        :param render:          Render on screen. Default false
        :param save_model:      Whether to save the model while training. Default None
        :return:                None
        """
        if experience:
            if self.network_type == network.Space:
                self.trainExpSpace(episodes, max_epsilon, min_epsilon, burn_in, render, save_model)
            else:
                self.trainExp(episodes, max_epsilon, min_epsilon, burn_in, render, save_model)
        else:
            self.trainNoExp(episodes, max_epsilon, min_epsilon, render, save_model)

    def trainNoExp(self, episodes, max_epsilon, min_epsilon, render, save_model):
        """
        Trains the model with no experience replay
        :param episodes:        Number of episodes to train for
        :param max_epsilon:     Max epsilon
        :param min_epsilon:     Min epsilon
        :param render:          Whether to render on screen
        :param save_model:      Where to save the model
        :return:                None
        """
        total_iters = 0

        for ep in range(episodes):
            state = np.expand_dims(self.env.reset(), axis=0)
            terminal = False
            epsilon = max_epsilon - (max_epsilon - min_epsilon) * ep / episodes
            total_reward = 0

            while not terminal:
                if total_iters % self.performance_counter == 0:
                    printVerbose("Total iterations: " + str(total_iters))
                    avg_reward = self.test(20, render=False)
                    summary = tf.Summary(value=[
                        tf.Summary.Value(tag="Performance", simple_value=avg_reward),
                    ])
                    self.DQN.writer.add_summary(summary, total_iters)

                action_values, action = self.epsilon_greedy_policy(state, epsilon)
                next_state, reward, terminal, _ = self.env.step(np.squeeze(action))
                next_state = np.expand_dims(next_state, axis=0)

                if render:
                    self.env.render()

                merged = self.DQN.trainDQN(state, action_values * (self.oneHot(action) ^ 1) \
                                  + self.oneHot(action) * (np.array(reward, ndmin=2) +
                                                           terminal * self.discount * self.DQN.getMaxValue(next_state)[
                                                               1]))

                state = next_state.copy()

                total_reward += reward
                total_iters += 1

            printVerbose("Episode: " + str(ep) + " Total reward: " + str(total_reward))
            summary = tf.Summary(value=[
                tf.Summary.Value(tag="Training reward", simple_value=total_reward),
            ])
            self.DQN.writer.add_summary(summary, ep)

            if save_model:
                self.DQN.save_model_weights(save_model)

    def trainExp(self, episodes, max_epsilon, min_epsilon, burn_in, render, save_model):
        """
        Trains the model with experience replay
        :param episodes:        Number of episodes to train for
        :param max_epsilon:     Max epsilon
        :param min_epsilon:     Min epsilon
        :param render:          Whether to render on screen
        :param save_model:      Where to save the model
        :return:                None
        """

        total_iters = 0

        self.burn_in_memory(burn_in)

        for ep in range(episodes):
            state = np.expand_dims(self.env.reset(), axis=0)
            terminal = False
            epsilon = max_epsilon - (max_epsilon - min_epsilon) * ep / episodes
            total_reward = 0

            while not terminal:
                if total_iters % self.performance_counter == 0:
                    printVerbose("Total iterations: " + str(total_iters))
                    avg_reward = self.test(20, render=False)
                    summary = tf.Summary(value=[
                        tf.Summary.Value(tag="Performance", simple_value=avg_reward),
                    ])
                    self.DQN.writer.add_summary(summary, total_iters)

                action_value, action = self.epsilon_greedy_policy(state, epsilon)
                next_state, reward, terminal, _ = self.env.step(np.squeeze(action))
                next_state = np.expand_dims(next_state, axis=0)

                if render:
                    self.env.render()

                self.memory.append(state, action, next_state, reward, not terminal)
                states, actions, next_states, rewards, terminals = self.memory.sample_batch()
                action_values = self.DQN.getValue(states, 0)

                merged = self.DQN.trainDQN(states, action_values * (self.oneHot(actions) ^ 1) \
                                  + self.oneHot(actions) * (np.array(rewards, ndmin=2) +
                                                            terminals * self.discount *
                                                            self.DQN.getMaxValue(next_states)[1]))

                state = next_state.copy()

                total_reward += reward
                total_iters += 1

            printVerbose("Episode: " + str(ep) + " Total reward: " + str(total_reward))
            summary = tf.Summary(value=[
                tf.Summary.Value(tag="Training reward", simple_value=total_reward),
            ])
            self.DQN.writer.add_summary(summary, ep)

            if save_model:
                self.DQN.save_model_weights(save_model)

    def trainExpSpace(self, episodes, max_epsilon, min_epsilon, burn_in, render, save_model):
        """
        Special version to train space invaders with memory
        :param episodes:        Number of episodes to train for
        :param max_epsilon:     Max epsilon
        :param min_epsilon:     Min epsilon
        :param render:          Whether to render on screen
        :param save_model:      Where to save the model
        :return:                None
        """
        total_iters = 0

        self.burn_in_memory_space(burn_in)

        for ep in range(episodes):
            frame = np.zeros((1, 105, 80, 3))
            state = self.processState(self.env.reset())[None, :, :, None]
            frame = np.append(frame, state, axis=3)

            epsilon = max_epsilon - (max_epsilon - min_epsilon) * ep / episodes

            val = 0
            total_reward = 0
            terminal = False

            while not terminal:
                if total_iters % self.performance_counter == 0:
                    printVerbose("Total iterations: " + str(total_iters))
                    avg_reward = self.testSpace(20, render=False)
                    summary = tf.Summary(value=[
                        tf.Summary.Value(tag="Performance", simple_value=avg_reward),
                    ])
                    self.DQN.writer.add_summary(summary, total_iters)

                action_value, action = self.epsilon_greedy_policy(frame, epsilon)
                next_state, reward, terminal, _ = self.env.step(np.squeeze(action))
                next_state = self.processState(next_state)[None, :, :, None]
                next_frame = np.append(frame[:, :, :, 1:], next_state, axis=3)

                if render:
                    self.env.render()

                self.memory.append(np.append(frame, next_state, axis=3), action, reward, not terminal)
                frames, actions, rewards, terminals = self.memory.sample_batch()
                action_values = self.DQN.getValue(frames[:,:,:,:-1], 0)

                merged = self.DQN.trainDQN(frames[:,:,:,:-1], action_values * (self.oneHot(actions) ^ 1)
                                  + self.oneHot(actions) * (np.array(rewards, ndmin=2) +
                                                            terminals * self.discount *
                                                            self.DQN.getMaxValue(frames[:,:,:,1:])[1]))
                self.DQN.writer.add_summary(merged, total_iters)

                frame = next_frame.copy()

                val += action_value
                total_iters += 1
                total_reward += reward

            printVerbose("Episode: " + str(ep) + " Total reward: " + str(total_reward))
            summary = tf.Summary(value=[
                tf.Summary.Value(tag="Training reward", simple_value=total_reward),
            ])
            self.DQN.writer.add_summary(summary, ep)


            if save_model:
                self.DQN.save_model_weights(save_model)

    def test(self, episodes=100, render=False):
        """
        Runs the policy for episodes and prints out average reward
        :param episodes:    Episodes to run for. Default 100
        :param render:      Whether to render. Default False
        :return:            None
        """
        total_reward = []

        for ep in range(episodes):
            state = np.expand_dims(self.test_env.reset(), axis=0)
            terminal = False
            reward = 0

            while not terminal:
                __, action = self.greedy_policy(state)

                next_state, r, terminal, _ = self.test_env.step(np.squeeze(action))
                next_state = np.expand_dims(next_state, axis=0)

                if render:
                    self.test_env.render()

                state = next_state.copy()
                reward += r

            total_reward.append(reward)

        printVerbose("Average reward across " + str(episodes) + " episodes: " + str(np.mean(total_reward)))
        return np.mean(total_reward)

    def testSpace(self, episodes=100, render=False):
        """
        Special version of test for Space invaders. Runs the policy for episodes and prints out average reward
        :param episodes:    Episodes to run for. Default 100
        :param render:      Whether to render. Default False
        :return:            None
        """
        total_rewards = []

        for ep in range(episodes):
            frame = np.zeros((1, 105, 80, 3))
            state = self.processState(self.test_env.reset())[None, :, :, None]
            frame = np.append(frame, state, axis=3)

            terminal = False
            r = 0

            while not terminal:
                __, action = self.greedy_policy(frame)
                next_state, reward, terminal, _ = self.test_env.step(np.squeeze(action))
                next_state = self.processState(next_state)[None, :, :, None]
                next_frame = np.append(frame[:, :, :, 1:], next_state, axis=3)

                if render:
                    self.test_env.render()

                frame = next_frame.copy()
                r += reward

            total_rewards.append(r)

        printVerbose("Average reward across " + str(episodes) + " episodes: " + str(np.mean(total_rewards)))
        return np.mean(total_rewards)

    def burn_in_memory(self, burn_in):
        """
        Function to burn into memory initial samples from policy
        :param burn_in:     Amount of samples to burn in
        :return:            None
        """
        state = np.expand_dims(self.env.reset(), axis=0)
        for i in range(burn_in):

            action_value, action = self.epsilon_greedy_policy(state, 0.05)
            next_state, reward, terminal, _ = self.env.step(np.squeeze(action))
            next_state = np.expand_dims(next_state, axis=0)

            self.memory.append(state, action, next_state, reward, not terminal)

            if terminal:
                state = np.expand_dims(self.env.reset(), axis=0)
            else:
                state = next_state.copy()

    def burn_in_memory_space(self, burn_in):
        """
        Special version to burn in for space invaders.
        :param burn_in:     Amount of samples to burn in
        :return:            None
        """
        frame = np.zeros((1, 105, 80, 3))
        state = self.processState(self.env.reset())[None, :, :, None]
        frame = np.append(frame, state, axis=3)

        for i in range(burn_in):

            action_value, action = self.epsilon_greedy_policy(frame, 0.05)
            next_state, reward, terminal, _ = self.env.step(np.squeeze(action))
            next_state = self.processState(next_state)[None, :, :, None]
            next_frame = np.append(frame[:, :, :, 1:], next_state, axis=3)

            self.memory.append(np.append(frame, next_state, axis=3), action, reward, not terminal)

            if terminal:
                frame = np.zeros((1, 105, 80, 3))
                state = self.processState(self.env.reset())[None, :, :, None]
                frame = np.append(frame, state, axis=3)
            else:
                frame = next_frame.copy()

    def oneHot(self, action):
        """
        Helper function to convert action to one-hot vector
        :param action:  action
        :return:        one hot vector of action
        """
        oneHot = np.zeros((action.shape[0], self.env.action_space.n), dtype=int)
        oneHot[np.arange(action.shape[0]), np.squeeze(action)] = 1
        return oneHot

    def processState(self, state):
        """
        Helper function to process the state for space invaders
        :param state:   input state
        :return:        processed stated
        """
        return np.mean(state[::2, ::2, :], axis=2)


def parse_arguments():
    parser = argparse.ArgumentParser(description='Deep Q Network Argument Parser')
    parser.add_argument('--env', dest='env', type=str)
    parser.add_argument('--render', dest='render', type=int, default=0)
    parser.add_argument('--train', dest='train', type=int, default=1)
    parser.add_argument('--learning_rate', dest='learning_rate', type=float)
    parser.add_argument('--discount', dest='discount', type=float)
    parser.add_argument('--test', dest='test', type=int, default=0)
    parser.add_argument('--network', dest='network', type=str)
    parser.add_argument('--load_model', dest='load_model', type=str, default=None)
    parser.add_argument('--save_model', dest='save_model', type=str, default=None)
    parser.add_argument('--episodes', dest='episodes', type=int, default=5000)
    parser.add_argument('--update_freq', dest='update_freq', type=int, default=1000)
    parser.add_argument('--max_epsilon', dest='max_epsilon', type=float, default=0.5)
    parser.add_argument('--min_epsilon', dest='min_epsilon', type=float, default=0.05)
    parser.add_argument('--exp', dest='exp', type=int, default=False)
    parser.add_argument('--exp_size', dest='exp_size', type=int, default=50000)
    parser.add_argument('--burn_in', dest='burn_in', type=int, default=10000)
    parser.add_argument('--exp_data_type', dest='exp_data_type', type=str, default='float')
    parser.add_argument('--monitor', dest='monitor', type=str, default=None)
    parser.add_argument('--performance_counter', dest='performance_counter', type=int, default=1000)

    parser.add_argument('--layers', dest='layers',
                        nargs="*",
                        type=int)
    parser.add_argument('--advantage_layers', dest='advantage_layers',
                        nargs="*",
                        type=int,
                        default=None)
    parser.add_argument('--value_layers', dest='value_layers',
                        nargs="*",
                        type=int,
                        default=None)
    parser.add_argument('--verbose', dest='verbose', type=int, default=1)

    return parser.parse_args()


def main(args):
    global verbose

    args = parse_arguments()
    environment_name = args.env
    render = args.render
    train = args.train
    test = args.test
    learning_rate = args.learning_rate
    discount = args.discount

    if args.network == "Linear":
        network_type = network.Linear
    elif args.network == "Deep":
        network_type = network.Deep
    elif args.network == "Double":
        network_type = network.Double
    elif args.network == "Dueling":
        network_type = network.Dueling
    elif args.network == "DDQN":
        network_type = network.DDQN
    elif args.network == "Space":
        network_type = network.Space

    load_model = args.load_model
    save_model = args.save_model
    episodes = args.episodes
    max_epsilon = args.max_epsilon
    min_epsilon = args.min_epsilon
    exp = args.exp
    burn_in = args.burn_in
    exp_size = args.exp_size
    performance_counter = args.performance_counter
    update_freq = args.update_freq

    if args.exp_data_type == "float":
        exp_data_type = np.float_
    elif args.exp_data_type == "uint8":
        exp_data_type = np.uint8

    monitor = args.monitor
    layers = args.layers
    advantage_layers = args.advantage_layers
    value_layers = args.value_layers
    verbose = args.verbose

    agent = DQN_Agent(environment_name,
                      network_type,
                      learning_rate,
                      layers,
                      update_freq,
                      advantage_layers=advantage_layers,
                      value_layers=value_layers,
                      discount=discount,
                      performance_counter=performance_counter,
                      monitor_dir=monitor,
                      data_type=exp_data_type,
                      memory_size=exp_size)
    agent.initDQN(load_model)
    if train:
        agent.train(episodes, max_epsilon, min_epsilon, exp, burn_in, render=render, save_model=save_model)
    if test:
        if network_type == network.Space:
            agent.testSpace(episodes, render=render)
        else:
            agent.test(episodes, render=render)


if __name__ == '__main__':
    main(sys.argv)
