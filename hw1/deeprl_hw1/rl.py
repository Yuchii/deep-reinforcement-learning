# coding: utf-8
from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import numpy as np
import gym
import deeprl_hw1.lake_envs as lake_env
import time
import matplotlib.pyplot as plt
import random

def print_policy(policy, action_names):
    """Print the policy in human-readable format.

    Parameters
    ----------
    policy: np.ndarray
      Array of state to action number mappings
    action_names: dict
      Mapping of action numbers to characters representing the action.
    """
    str_policy = policy.astype('str')
    for action_num, action_name in action_names.items():
        np.place(str_policy, policy == action_num, action_name)

    print(str_policy)


def print_value(value):
    for v in range(len(value)):
        print("State: " + str(v) + " Value: " + str(value[v]))



def value_function_to_policy(env, gamma, value_function):
    """Output action numbers for each state in value_function.

    Parameters
    ----------
    env: gym.core.Environment
      Environment to compute policy for. Must have nS, nA, and P as
      attributes.
    gamma: float
      Discount factor. Number in range [0, 1)
    value_function: np.ndarray
      Value of each state.

    Returns
    -------
    np.ndarray
      An array of integers. Each integer is the optimal action to take
      in that state according to the environment dynamics and the
      given value function.
    """

    policy = np.array([np.argmax([np.sum([ns[0] * (ns[2] + gamma * (value_function[ns[1]] if ns[3] is False else 0))
                                 for ns in env.P[s][a]]) for a in range(env.nA)]) for s in range(env.nS)])
    return policy


def evaluate_policy_sync(env, gamma, policy, max_iterations=int(1e3), tol=1e-3):
    """Performs policy evaluation.
    
    Evaluates the value of a given policy.

    Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    policy: np.array
      The policy to evaluate. Maps states to actions.
    max_iterations: int
      The maximum number of iterations to run before stopping.
    tol: float
      Determines when value function has converged.

    Returns
    -------
    np.ndarray, int
      The value for the given policy and the number of iterations till
      the value function converged.
    """
    #P[s][a] = [(prob, nextstate, reward, is_terminal), ...]

    # values = np.random.normal(0, 1, env.nS)
    values = np.zeros(env.nS)

    for i in range(max_iterations):
        new_values = values.copy()
        max_diff = 0

        for s in range(env.nS):
            new_values[s] = np.sum([ns[0] * (ns[2] + gamma * values[ns[1]]) for ns in env.P[s][policy[s]]])
            max_diff = max(max_diff, abs(new_values[s] - values[s]))

        values = new_values

        if max_diff < tol:
            break

    return values, i + 1


def evaluate_policy_async_ordered(env, gamma, policy, max_iterations=int(1e3), tol=1e-3):
    """Performs policy evaluation.
    
    Evaluates the value of a given policy by asynchronous DP.  Updates states in
    their 1-N order.

    Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    policy: np.array
      The policy to evaluate. Maps states to actions.
    max_iterations: int
      The maximum number of iterations to run before stopping.
    tol: float
      Determines when value function has converged.

    Returns
    -------
    np.ndarray, int
      The value for the given policy and the number of iterations till
      the value function converged.
    """
    values = np.zeros(env.nS)

    for i in range(max_iterations):
        max_diff = 0

        for s in range(env.nS):
            old_value = values[s]
            values[s] = np.sum([ns[0] * (ns[2] + gamma * values[ns[1]]) for ns in env.P[s][policy[s]]])
            max_diff = max(max_diff, abs(old_value - values[s]))

        if max_diff < tol:
            break

    return values, i + 1


def evaluate_policy_async_randperm(env, gamma, policy, max_iterations=int(1e3), tol=1e-3):
    """Performs policy evaluation.
    
    Evaluates the value of a policy.  Updates states by randomly sampling index
    order permutations.

    Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    policy: np.array
      The policy to evaluate. Maps states to actions.
    max_iterations: int
      The maximum number of iterations to run before stopping.
    tol: float
      Determines when value function has converged.

    Returns
    -------
    np.ndarray, int
      The value for the given policy and the number of iterations till
      the value function converged.
    """
    values = np.zeros(env.nS)


    for i in range(max_iterations):
        max_diff = 0
        s_list = list(range(env.nS))
        random.shuffle(s_list)

        for s in s_list:
            old_value = values[s]
            values[s] = np.sum([ns[0] * (ns[2] + gamma * values[ns[1]]) for ns in env.P[s][policy[s]]])
            max_diff = max(max_diff, abs(old_value - values[s]))

        if max_diff < tol:
            break

    return values, i + 1

def evaluate_policy_async_custom(env, gamma, policy, max_iterations=int(1e3), tol=1e-3):
    """Performs policy evaluation.
    
    Evaluate the value of a policy. Updates states by a student-defined
    heuristic. 

    Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    policy: np.array
      The policy to evaluate. Maps states to actions.
    max_iterations: int
      The maximum number of iterations to run before stopping.
    tol: float
      Determines when value function has converged.

    Returns
    -------
    np.ndarray, int
      The value for the given policy and the number of iterations till
      the value function converged.
    """
    return np.zeros(env.nS), 0


def improve_policy(env, gamma, value_func, policy):
    """Performs policy improvement.
    
    Given a policy and value function, improves the policy.

    Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    value_func: np.ndarray
      Value function for the given policy.
    policy: dict or np.array
      The policy to improve. Maps states to actions.

    Returns
    -------
    bool, np.ndarray
      Returns true if policy changed. Also returns the new policy.
    """

    #P[s][a] = [(prob, nextstate, reward, is_terminal), ...]

    stable = True
    for s in range(env.nS):
        old_action = policy[s]

        values = [np.sum([ns[0] * (ns[2] + gamma * value_func[ns[1]]) for ns in env.P[s][a]]) for a in range(env.nA)]

        if np.argmax(values) != old_action:
            stable = False

        policy[s] = np.argmax(values)

    return stable, policy


def policy_iteration_sync(env, gamma, max_iterations=int(1e3), tol=1e-3):
    """Runs policy iteration.

    See page 85 of the Sutton & Barto Second Edition book.

    You should use the improve_policy() and evaluate_policy_sync() methods to
    implement this method.
    
    Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    max_iterations: int
      The maximum number of iterations to run before stopping.
    tol: float
      Determines when value function has converged.

    Returns
    -------
    (np.ndarray, np.ndarray, int, int)
       Returns optimal policy, value function, number of policy
       improvement iterations, and number of value iterations.
    """
    policy = np.zeros(env.nS)
    value_func = np.zeros(env.nS)
    total_eval_steps = 0

    for i in range(max_iterations):
        value_func, eval_i = evaluate_policy_sync(env, gamma, policy)
        stable, policy = improve_policy(env, gamma, value_func, policy)
        total_eval_steps += eval_i
        if stable:
            break


    return policy, value_func, i + 1, total_eval_steps


def policy_iteration_async_ordered(env, gamma, max_iterations=int(1e3),
                                   tol=1e-3):
    """Runs policy iteration.

    You should use the improve_policy and evaluate_policy_async_ordered methods
    to implement this method.

    Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    max_iterations: int
      The maximum number of iterations to run before stopping.
    tol: float
      Determines when value function has converged.

    Returns
    -------
    (np.ndarray, np.ndarray, int, int)
       Returns optimal policy, value function, number of policy
       improvement iterations, and number of value iterations.
    """
    policy = np.zeros(env.nS)
    value_func = np.zeros(env.nS)
    total_eval_steps = 0

    for i in range(max_iterations):
        value_func, eval_i = evaluate_policy_async_ordered(env, gamma, policy)
        stable, policy = improve_policy(env, gamma, value_func, policy)
        total_eval_steps += eval_i
        if stable:
            break

    return policy, value_func, i + 1, total_eval_steps


def policy_iteration_async_randperm(env, gamma, max_iterations=int(1e3),
                                    tol=1e-3):
    """Runs policy iteration.

    You should use the improve_policy and evaluate_policy_async_randperm methods
    to implement this method.

    Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    max_iterations: int
      The maximum number of iterations to run before stopping.
    tol: float
      Determines when value function has converged.

    Returns
    -------
    (np.ndarray, np.ndarray, int, int)
       Returns optimal policy, value function, number of policy
       improvement iterations, and number of value iterations.
    """
    policy = np.zeros(env.nS)
    value_func = np.zeros(env.nS)
    total_eval_steps = 0

    for i in range(max_iterations):
        value_func, eval_i = evaluate_policy_async_randperm(env, gamma, policy)
        stable, policy = improve_policy(env, gamma, value_func, policy)
        total_eval_steps += eval_i
        if stable:
            break


    return policy, value_func, i + 1, total_eval_steps


def policy_iteration_async_custom(env, gamma, max_iterations=int(1e3),
                                  tol=1e-3):
    """Runs policy iteration.

    You should use the improve_policy and evaluate_policy_async_custom methods
    to implement this method.

    Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    max_iterations: int
      The maximum number of iterations to run before stopping.
    tol: float
      Determines when value function has converged.

    Returns
    -------
    (np.ndarray, np.ndarray, int, int)
       Returns optimal policy, value function, number of policy
       improvement iterations, and number of value iterations.
    """
    policy = np.random.uniform(0, 4, env.nS, dtype='int')
    value_func = np.random.normal(0, 1, env.nS)

    return policy, value_func, 0, 0


def value_iteration_sync(env, gamma, max_iterations=int(1e3), tol=1e-3):
    """Runs value iteration for a given gamma and environment.

    Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    max_iterations: int
      The maximum number of iterations to run before stopping.
    tol: float
      Determines when value function has converged.

    Returns
    -------
    np.ndarray, iteration
      The value function and the number of iterations it took to converge.
    """
    values = np.zeros(env.nS)
    i = 0

    for i in range(max_iterations):
        new_values = values.copy()
        max_diff = 0

        for s in range(env.nS):
            new_values[s] = np.max([np.sum([ns[0] * (ns[2] + gamma * values[ns[1]])
                                            for ns in env.P[s][a]]) for a in range(env.nA)])
            max_diff = max(max_diff, abs(new_values[s] - values[s]))

        values = new_values

        if max_diff < tol:
            break

    return values, i + 1


def value_iteration_async_ordered(env, gamma, max_iterations=int(1e3), tol=1e-3):
    """Runs value iteration for a given gamma and environment.
    Updates states in their 1-N order.

    Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    max_iterations: int
      The maximum number of iterations to run before stopping.
    tol: float
      Determines when value function has converged.

    Returns
    -------
    np.ndarray, iteration
      The value function and the number of iterations it took to converge.
    """
    values = np.zeros(env.nS)
    i = 0

    for i in range(max_iterations):
        max_diff = 0

        for s in range(env.nS):
            old_value = values[s]
            values[s] = np.max([np.sum([ns[0] * (ns[2] + gamma * values[ns[1]])
                                            for ns in env.P[s][a]]) for a in range(env.nA)])
            max_diff = max(max_diff, abs(old_value - values[s]))

        if max_diff < tol:
            break

    return values, i + 1



def value_iteration_async_randperm(env, gamma, max_iterations=int(1e3),
                                   tol=1e-3):
    """Runs value iteration for a given gamma and environment.
    Updates states by randomly sampling index order permutations.

    Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    max_iterations: int
      The maximum number of iterations to run before stopping.
    tol: float
      Determines when value function has converged.

    Returns
    -------
    np.ndarray, iteration
      The value function and the number of iterations it took to converge.
    """
    values = np.zeros(env.nS)
    i = 0

    for i in range(max_iterations):
        max_diff = 0
        s_list = list(range(env.nS))
        random.shuffle(s_list)

        for s in s_list:
            old_value = values[s]
            values[s] = np.max([np.sum([ns[0] * (ns[2] + gamma * values[ns[1]])
                                            for ns in env.P[s][a]]) for a in range(env.nA)])
            max_diff = max(max_diff, abs(old_value - values[s]))


        if max_diff < tol:
            break

    return values, i + 1

def value_iteration_async_custom(env, gamma, max_iterations=int(1e3), tol=1e-3):
    """Runs value iteration for a given gamma and environment.
    Updates states by student-defined heuristic.

    Parameters
    ----------
    env: gym.core.Environment
      The environment to compute value iteration for. Must have nS,
      nA, and P as attributes.
    gamma: float
      Discount factor, must be in range [0, 1)
    max_iterations: int
      The maximum number of iterations to run before stopping.
    tol: float
      Determines when value function has converged.

    Returns
    -------
    np.ndarray, iteration
      The value function and the number of iterations it took to converge.
    """

    if env.nS == 16:
        goal = 0
    elif env.nS == 64:
        goal = 57
    values = np.zeros(env.nS)
    s_list = list(range(env.nS))
    l1_dist = s_list // np.sqrt(env.nS) - (goal // np.sqrt(env.nS)) + s_list % np.sqrt(env.nS) - goal % np.sqrt(env.nS)

    dist_list = list(zip(s_list, l1_dist))
    dist_list.sort(key=lambda x: x[1])

    for i in range(max_iterations):
        max_diff = 0
        for s in [x[0] for x in dist_list[::1]][:int(env.nS/2)]:
            old_value = values[s]
            values[s] = np.max([np.sum([ns[0] * (ns[2] + gamma * values[ns[1]])
                                        for ns in env.P[s][a]]) for a in range(env.nA)])
            max_diff = max(max_diff, abs(old_value - values[s]))

        for s in [x[0] for x in dist_list[::1]][:int(2 * env.nS/3)]:
            old_value = values[s]
            values[s] = np.max([np.sum([ns[0] * (ns[2] + gamma * values[ns[1]])
                                        for ns in env.P[s][a]]) for a in range(env.nA)])
            max_diff = max(max_diff, abs(old_value - values[s]))

        for s in [x[0] for x in dist_list[::1]][:]:
            old_value = values[s]
            values[s] = np.max([np.sum([ns[0] * (ns[2] + gamma * values[ns[1]])
                                        for ns in env.P[s][a]]) for a in range(env.nA)])
            max_diff = max(max_diff, abs(old_value - values[s]))

        if max_diff < tol:
            break

    return values, i + 1


def execute_policy(env, policy, iterations, gamma):

    state = env.reset()
    total_reward = 0
    for i in range(iterations):
        discount = 1
        state = env.reset()

        while True:

            state, reward, is_terminal, debug_info = env.step(policy[state])
            # env.render()

            total_reward += discount * reward
            discount *= gamma

            if is_terminal:
                break

    print("Average reward: " + str(total_reward/iterations))

def main():

    #Problem 2, Part I: Deterministic Environment
    # print("Problem 2, Part I: Deterministic Environment 4x4 Policy iteration")
    # env = gym.make('Deterministic-4x4-FrozenLake-v0')
    #
    # start_time = time.time()
    # policy, value_func, improv_steps, total_eval_steps= policy_iteration_sync(env, 0.9)
    # elapsed_time = time.time() - start_time
    # # print_value(value_func)
    # print_policy(policy, {0: 'L', 2: 'R', 1: 'D', 3: 'U'})
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + " Total # Eval Steps: " + str(total_eval_steps) + "\n")
    # plt.imshow(value_func.reshape((4, 4)), cmap = 'hot', interpolation = None)
    # plt.colorbar()
    # plt.title("Heatmap for value function of 4x4 Frozen Lake \n with synchronous policy iteration")
    # plt.show()
    # #
    # print("Problem 2, Part I: Deterministic Environment 8x8 Policy iteration")
    # env = gym.make('Deterministic-8x8-FrozenLake-v0')
    # start_time = time.time()
    # policy, value_func, improv_steps, total_eval_steps= policy_iteration_sync(env, 0.9)
    # elapsed_time = time.time() - start_time
    # print_policy(policy, {0: 'L', 2: 'R', 1: 'D', 3: 'U'})
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + " Total # Eval Steps: " + str(total_eval_steps) + "\n")
    # plt.imshow(value_func.reshape((8, 8)), cmap = 'hot', interpolation = None)
    # plt.colorbar()
    # plt.title("Heatmap for value function of 8x8 Frozen Lake \n with synchronous policy iteration")
    # plt.show()
    #
    #
    # print("Problem 2, Part I: Deterministic Environment 4x4 Value iteration")
    # env = gym.make('Deterministic-4x4-FrozenLake-v0')
    # start_time = time.time()
    # value_func, improv_steps= value_iteration_sync(env, 0.9)
    # elapsed_time = time.time() - start_time
    # print_value(value_func)
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + "\n")
    # plt.imshow(value_func.reshape((4, 4)), cmap = 'hot', interpolation = None)
    # plt.colorbar()
    # plt.title("Heatmap for value function of 4x4 Frozen Lake \n with synchronous value iteration")
    # plt.show()
    # print("4x4 Value to policy")
    # policy = value_function_to_policy(env, 0.9, value_func)
    # print_policy(policy, {0: 'L', 2: 'R', 1: 'D', 3: 'U'})
    # print("\n")
    # #
    # print("Problem 2, Part I: Deterministic Environment 8x8 Value iteration")
    # env = gym.make('Deterministic-8x8-FrozenLake-v0')
    # start_time = time.time()
    # value_func, improv_steps= value_iteration_sync(env, 0.9)
    # elapsed_time = time.time() - start_time
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + "\n")
    # plt.imshow(value_func.reshape((8, 8)), cmap = 'hot', interpolation = None)
    # plt.colorbar()
    # plt.title("Heatmap for value function of 8x8 Frozen Lake \n with synchronous value iteration")
    # plt.show()
    #
    #
    # print("asynchronous policy iteration ordered")
    # env = gym.make('Deterministic-8x8-FrozenLake-v0')
    # start_time = time.time()
    # policy, value_func, improv_steps, total_eval_steps = policy_iteration_async_ordered(env, 0.9)
    # elapsed_time = time.time() - start_time
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + " Total # Eval Steps: " + str(total_eval_steps) + "\n")
    # plt.imshow(value_func.reshape((8, 8)), cmap = 'hot', interpolation = None)
    # plt.colorbar()
    # plt.title("Value iteration heatmap for value function of 4x4 Frozen Lake")
    # plt.show()
    # print("4x4 Value to policy")
    # policy = value_function_to_policy(env, 0.9, value_func)
    # print_policy(policy, {0: 'L', 2: 'R', 1: 'D', 3: 'U'})
    # print("\n")
    #
    # print("asynchronous policy iteration random")
    # env = gym.make('Deterministic-8x8-FrozenLake-v0')
    # start_time = time.time()
    # policy, value_func, improv_steps, total_eval_steps = policy_iteration_async_randperm(env, 0.9)
    # elapsed_time = time.time() - start_time
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + " Total # Eval Steps: " + str(total_eval_steps) + "\n")
    # plt.imshow(value_func.reshape((8, 8)), cmap = 'hot', interpolation = None)
    # plt.colorbar()
    # plt.title("Value iteration heatmap for value function of 4x4 Frozen Lake")
    # plt.show()
    # print("4x4 Value to policy")
    # policy = value_function_to_policy(env, 0.9, value_func)
    # print_policy(policy, {0: 'L', 2: 'R', 1: 'D', 3: 'U'})
    # print("\n")
    #
    # env = gym.make('Deterministic-4x4-FrozenLake-v0')
    # value_func, improv_steps = value_iteration_async_ordered(env, 0.9)
    # policy = value_function_to_policy(env, 0.9, value_func)
    # execute_policy(env, policy, 1, 0.9)
    # print("Simulated value: " + str(value_func[env.reset()]))
    #
    # print("Async Value iteration ordered 8x8")
    # env = gym.make('Deterministic-8x8-FrozenLake-v0')
    # start_time = time.time()
    # value_func, improv_steps= value_iteration_async_ordered(env, 0.9)
    # elapsed_time = time.time() - start_time
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + "\n")
    # plt.imshow(value_func.reshape((8, 8)), cmap = 'hot', interpolation = None)
    # plt.colorbar()
    # plt.title("Async Value iteration ordered heatmap for value function of 8x8 Frozen Lake")
    # plt.show()
    #
    # policy = value_function_to_policy(env, 0.9, value_func)
    # execute_policy(env, policy, 1, 0.9)
    # print("Simulated value: " + str(value_func[env.reset()]))
    #
    # print("Async Value iteration rand 8x8")
    # start_time = time.time()
    # value_func, improv_steps= value_iteration_async_randperm(env, 0.9)
    # elapsed_time = time.time() - start_time
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + "\n")
    # plt.imshow(value_func.reshape((8, 8)), cmap = 'hot', interpolation = None)
    # plt.colorbar()
    # plt.title("Async Value iteration rand heatmap for value function of 8x8 Frozen Lake")
    # plt.show()
    #
    # print("Stochastic Environment 4x4 Synchronous Value iteration")
    # env = gym.make('Stochastic-4x4-FrozenLake-v0')
    # start_time = time.time()
    # value_func, improv_steps= value_iteration_sync(env, 0.9)
    # elapsed_time = time.time() - start_time
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + "\n")
    # plt.imshow(value_func.reshape((4, 4)), cmap = 'hot', interpolation = None)
    # plt.colorbar()
    # plt.title("Heatmap for value function of stochastic 4x4 Frozen Lake \n with synchronous value iteration")
    # plt.show()
    # print("4x4 Value to policy")
    # policy = value_function_to_policy(env, 0.9, value_func)
    # print_policy(policy, {0: 'L', 2: 'R', 1: 'D', 3: 'U'})
    # print("\n")
    #
    # print("Stochastic Environment 8x8 Synchronous Value iteration")
    # env = gym.make('Stochastic-8x8-FrozenLake-v0')
    # start_time = time.time()
    # value_func, improv_steps= value_iteration_sync(env, 0.9)
    # elapsed_time = time.time() - start_time
    # policy = value_function_to_policy(env, 0.9, value_func)
    # print_policy(policy, {0: 'L', 2: 'R', 1: 'D', 3: 'U'})
    # #
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + "\n")
    # plt.imshow(value_func.reshape((8, 8)), cmap = 'hot', interpolation = None)
    # plt.colorbar()
    # plt.title("Heatmap for value function of stochastic 8x8 Frozen Lake \n with synchronous value iteration")
    # plt.show()
    # print("Deterministic Environment 4x4 Asynchronous Value iteration ordered")
    # env = gym.make('Deterministic-4x4-FrozenLake-v0')
    #
    # start_time = time.time()
    # value_func, improv_steps= value_iteration_async_ordered(env, 0.9)
    # elapsed_time = time.time() - start_time
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + "\n")
    #
    # print("Stochastic Environment 4x4 Asynchronous Value iteration ordered")
    # env = gym.make('Stochastic-4x4-FrozenLake-v0')
    #
    # start_time = time.time()
    # value_func, improv_steps= value_iteration_async_ordered(env, 0.9)
    # elapsed_time = time.time() - start_time
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + "\n")
    #
    # print("Deterministic Environment 8x8 Asynchronous Value iteration ordered")
    # env = gym.make('Deterministic-8x8-FrozenLake-v0')
    #
    # start_time = time.time()
    # value_func, improv_steps= value_iteration_async_ordered(env, 0.9)
    # elapsed_time = time.time() - start_time
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + "\n")
    #
    # print("Stochastic Environment 8x8 Asynchronous Value iteration ordered")
    # env = gym.make('Stochastic-8x8-FrozenLake-v0')
    #
    # start_time = time.time()
    # value_func, improv_steps= value_iteration_async_ordered(env, 0.9)
    # elapsed_time = time.time() - start_time
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + "\n")
    #
    # print("Deterministic Environment 4x4 Asynchronous Value iteration custom")
    # env = gym.make('Deterministic-4x4-FrozenLake-v0')
    # start_time = time.time()
    # value_func, improv_steps= value_iteration_async_custom(env, 0.9)
    # elapsed_time = time.time() - start_time
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + "\n")
    # plt.imshow(value_func.reshape((4, 4)), cmap = 'hot', interpolation = None)
    # plt.colorbar()
    # plt.title("Value iteration heatmap for value function of 4x4 Frozen Lake")
    # plt.show()
    #
    # print("Stochastic Environment 4x4 Asynchronous Value iteration custom")
    # env = gym.make('Stochastic-4x4-FrozenLake-v0')
    # start_time = time.time()
    # value_func, improv_steps= value_iteration_async_custom(env, 0.9)
    # elapsed_time = time.time() - start_time
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + "\n")
    # plt.imshow(value_func.reshape((4, 4)), cmap = 'hot', interpolation = None)
    # plt.colorbar()
    # plt.title("Value iteration heatmap for value function of 4x4 Frozen Lake")
    # plt.show()

    # value_func, improv_steps= value_iteration_sync(env, 0.9)
    # policy = value_function_to_policy(env, 0.9, value_func)
    # execute_policy(env, policy, 50000, 0.9)
    # print("Simulated value: " + str(value_func[env.reset()]))


    # print("Deterministic Environment 8x8 Asynchronous Value iteration custom")
    # env = gym.make('Deterministic-8x8-FrozenLake-v0')
    # start_time = time.time()
    # value_func, improv_steps= value_iteration_async_custom(env, 0.9)
    # elapsed_time = time.time() - start_time
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + "\n")
    # plt.imshow(value_func.reshape((8, 8)), cmap = 'hot', interpolation = None)
    # plt.colorbar()
    # plt.title("Value iteration heatmap for value function of 8x8 Frozen Lake")
    # plt.show()
    # #
    # print("Stochastic Environment 8x8 Asynchronous Value iteration custom")
    # env = gym.make('Stochastic-8x8-FrozenLake-v0')
    # start_time = time.time()
    # value_func, improv_steps = value_iteration_async_custom(env, 0.9)
    # elapsed_time = time.time() - start_time
    # print("Elapsed time: " + str(elapsed_time) + " #Policy Improvement steps: " + str(improv_steps) + "\n")
    # plt.imshow(value_func.reshape((8, 8)), cmap='hot', interpolation=None)
    # plt.colorbar()
    # plt.title("Value iteration heatmap for value function of 8x8 Frozen Lake")
    # plt.show()

    # policy = value_function_to_policy(env, 0.9, value_func)
    # execute_policy(env, policy, 50000, 0.9)
    # print("Simulated value: " + str(value_func[env.reset()]))

if __name__ == '__main__':
    main()